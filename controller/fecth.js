const conn = require("../db/connection");

module.exports = {
  fecth_province: (Request, Response) => {
    conn.query(
      //select * from province;
      "CALL barn_mueng_keng.fecth_province()",
      (error, results, fields) => {
        if (error) {
          return Response.status(200).send("ERRROR: Integer only");
        }
        Response.send(results[0]);
      }
    );
  },
  fecth_dristirc: (Request, Response) => {
    const params = Request.query;
    conn.query(
      //select * from dristric where pr_id = ?;
      "CALL barn_mueng_keng.fecth_dristric(?)",
      params.pr_id,
      (error, results, fields) => {
        if (error) {
          return Response.status(200).send("ERRROR");
        }
        Response.send(results[0]);
      }
    );
  },
  fecth_village: (Request, Response) => {
    const params = Request.query;
    conn.query(
      //select * from village where dr_id = ?;
      "CALL barn_mueng_keng.fecth_village(?)",
      params.dr_id,
      (error, results, fields) => {
        if (error) {
          return Response.status(200).send("ERRROR");
        }
        Response.send(results[0]);
      }
    );
  },
  fecth_dris_and_provi: (Request, Response) => {
    const params = Request.query;
    conn.query(
      //select province.pr_name, dristric.dr_name from dristric
      //inner join province on dristric.pr_id = province.pr_id where dristric.dr_id =
      //(select village.dr_id from village where village.vill_id = ?);
      "CALL barn_mueng_keng.fecth_dris_and_pro(?)",
      params.vill_id,
      (error, results, fields) => {
        if (error) {
          return Response.status(200).send("ERRROR");
        }
        Response.send(results[0]);
      }
    );
  },
};
