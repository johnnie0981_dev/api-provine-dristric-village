const fecth = require("../controller/fecth");
module.exports = (app) => {
  app
    .route("/api/fech/province-dristric-village/laos")
    .get(fecth.fecth_province);
  app
    .route("/api/fech/province-dristric-village/laos/dristric")
    .get(fecth.fecth_dristirc);
  app
    .route("/api/fech/province-dristric-village/laos/village")
    .get(fecth.fecth_village);
  app
    .route("/api/fech/province-dristric-village/laos/vill_id")
    .get(fecth.fecth_dris_and_provi);
};
